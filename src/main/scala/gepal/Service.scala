package gepal

import gepal.Model._

object Model {
  type Identifier = String
  type AccountIdentifier = Identifier
  type PersonIdentifier = Identifier
  type AgreementIdentifier = Identifier
  type Balance = BigDecimal // Could be with Currency

  type Name = String
  type Index = Int

  case class Agreement(agreementId: Identifier, accountId: Identifier, accountType: AccountType)

  case class Person(personId: Identifier, name: Name)

  case class Account(accountId: AccountIdentifier, accountType: AccountType, name: Name, balance: Balance)

  sealed trait AccountType extends Ordered[AccountType] {
    override def compare(that: AccountType): Index = {
      val indexes: Map[AccountType, Index] =
          Seq(CurrentAccount, SavingsAccount, JuniorAccount).zipWithIndex.toMap
      indexes(that) - indexes(this)
    }
  }

  case object CurrentAccount extends AccountType

  case object SavingsAccount extends AccountType

  case object JuniorAccount extends AccountType

  // TODO use base class
  case class AccountNotFound(identifier: Identifier) extends Exception

  //  case class PersonNotFound(identifier: Identifier) extends Exception
}

trait AccountBalanceService {
  def getBalance(account: AccountIdentifier): Balance
}

trait AccountNameService {
  def getName(account: AccountIdentifier): Name
}

trait AgreementService {
  def getAgreements(person: PersonIdentifier): Set[Agreement]
}

trait PreferencesService {
  // A Person can have a preferredAccount
  def getPreferredAccount(person: PersonIdentifier): Option[AccountIdentifier]
}