package gepal

import gepal.Model._

trait AccountsService {
  def getAccounts(person: Person): Seq[Account]
}


trait Accounts extends AccountsService with AgreementService with AccountBalanceService with AccountNameService with PreferencesService {
  override def getAccounts(person: Person): Seq[Account] = {
    val agreements: Set[Agreement] = getAgreements(person.personId)

    val accounts: Set[Account] = agreements.map {
      agreement => {
        val name: Name = getName(agreement.accountId)
        val balance: Balance = getBalance(agreement.accountId)
        Account(agreement.accountId, agreement.accountType, name, balance)
      }
    }

    val preferredAccountId = getPreferredAccount(person.personId)
    sortAccounts(accounts, preferredAccountId)
  }

  @inline
  def sortAccounts(accounts: Set[Account],
                   preferredAccountIdentifier: Option[AccountIdentifier]): Seq[Account] = {
    // sort first
    val sortedAccounts: Seq[Account] = accounts.toSeq.sortBy {
      case Account(accountId, accountType, name, _) =>
        (preferredAccountIdentifier.exists(_ != accountId)
          , accountId
          , accountType
          , name)
    }
    sortedAccounts
  }
}
