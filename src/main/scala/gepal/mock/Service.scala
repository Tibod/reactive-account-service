package gepal.mock

import gepal.Model._
import gepal.{AccountBalanceService, AccountNameService, AgreementService, PreferencesService}

trait MockAgreementService extends AgreementService {
  override def getAgreements(person: PersonIdentifier): Set[Agreement] =
    person match {
      case "person1" => Set(
        Agreement("agreement1", "IBAN1", CurrentAccount),
        Agreement("agreement2", "IBAN2", CurrentAccount))
    }
}

trait MockAccountBalanceService extends AccountBalanceService {
  override def getBalance(account: AccountIdentifier): Balance =
    account match {
      case "IBAN1" => 100
      case "IBAN2" => 50
      case _ => throw new AccountNotFound(account)
    }
}

trait MockAccountNameService extends AccountNameService {
  override def getName(account: AccountIdentifier): Name =
    account match {
      case "IBAN1" => "Current Account"
      case "IBAN2" => "Savings Account"
      case _ => throw new AccountNotFound(account)
    }
}

trait MockPreferencesService extends PreferencesService {
  override def getPreferredAccount(person: PersonIdentifier): Option[AccountIdentifier] = {
    Some("IBAN2")
  }
}
