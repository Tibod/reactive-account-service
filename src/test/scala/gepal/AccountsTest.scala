package gepal

import gepal.Model._
import gepal.mock.{MockAccountBalanceService, MockAccountNameService, MockAgreementService, MockPreferencesService}
import org.scalacheck.Arbitrary._
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.prop.PropertyChecks
import org.scalatest.{FlatSpec, Matchers, ParallelTestExecution}

import scala.util.Random


class AccountsTest extends FlatSpec with Matchers with PropertyChecks with ParallelTestExecution {

  behavior of "AccountService"

  val accountsService = new Accounts with MockAccountBalanceService with MockAccountNameService with MockAgreementService with MockPreferencesService

  it should "return the mocked values" in {
    val accounts: Seq[Account] = accountsService.getAccounts(Person("person1", "Tim"))

    accounts should contain allOf(Account("IBAN1", CurrentAccount, "Current Account", 100), Account("IBAN2", CurrentAccount, "Savings Account", 50))
  }

  it should "have the correct prefered order" in {
    val accounts: Seq[Account] = accountsService.getAccounts(Person("person1", "Tim"))

    accounts should contain inOrderOnly(Account("IBAN2", CurrentAccount, "Savings Account", 50), Account("IBAN1", CurrentAccount, "Current Account", 100))
  }

  behavior of "List of Accounts"

  it should "always put the preferred account up front" in {
    forAll { (input: Seq[Account]) =>
      whenever (input.nonEmpty) {
        val randomItem = input(Random.nextInt(input.size))
        val inputSet = input.toSet
        accountsService.sortAccounts(inputSet, Some(randomItem.accountId)).head should equal(randomItem)
      }
    }
  }

  it should "be sorted by accountId" in {
    forAll { (input: Set[Account]) =>
      val sorted = input.toSeq.sortBy(_.accountId)
      accountsService.sortAccounts(input, None) should equal(sorted)
    }
  }

  def accountType: Gen[AccountType] = Gen.oneOf(CurrentAccount, SavingsAccount, JuniorAccount)

  implicit lazy val accountTypeArbitrary: Arbitrary[AccountType] = Arbitrary(accountType)

  def agreementGenerator: Gen[Agreement] = for {
    agreementId <- arbitrary[Identifier]
    accountId <- arbitrary[Identifier]
    accountType <- arbitrary[AccountType]
  } yield Agreement(agreementId, accountId, accountType)


  implicit lazy val agreementArbitrary: Arbitrary[Agreement] = Arbitrary(agreementGenerator)

  def personGenerator: Gen[Person] = for {
    agreementId <- arbitrary[Identifier]
    name <- arbitrary[Name]
  } yield Person(agreementId, name)

  implicit lazy val persibsArbitrary: Arbitrary[Person] = Arbitrary(personGenerator)

  def accountGenerator: Gen[Account] = for {
    accountId <- arbitrary[Identifier]
    if accountId.length >= 1
    accountType <- arbitrary[AccountType]
    name <- arbitrary[Name]
    if name.length >= 1
    balance <- arbitrary[Int]

  } yield Account(accountId, accountType, name, BigDecimal(balance))

  implicit lazy val accountArbitrary: Arbitrary[Account] = Arbitrary(accountGenerator)


  //  it should "generate random stuff" in {
  //    forAll { account: Account =>
  //      println(account)
  //    }
  //  }


}

