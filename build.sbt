import AssemblyKeys._

assemblySettings

jarName in assembly := "exercise1.jar"

name := "gepal"

version := "1.0"

libraryDependencies ++= {
  val akkaV = "2.3.0"
  val sprayV = "1.3.1"
  Seq(
    "com.github.tototoshi" %% "scala-csv" % "1.0.0",
    "com.github.nscala-time" %% "nscala-time" % "1.2.0",
    "com.scalatags" %% "scalatags" % "0.4.0",
    "io.spray" % "spray-can" % sprayV,
    "io.spray" % "spray-routing" % sprayV,
    //    "io.spray" % "spray-testkit" % sprayV % "test",
        "com.typesafe.akka" %% "akka-actor" % akkaV,
    //    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "org.specs2" %% "specs2-core" % "2.3.7" % "test",
    "org.scalatest" %% "scalatest" % "2.1.7" % "test",
    "org.scalacheck" %% "scalacheck" % "1.11.4" % "test"
//    "org.typelevel" %% "scalaz-scalatest" % "0.2.2" % "test"
  )
}

